from typing import Sequence, Dict, Callable, Set
from functools import reduce
from operator import add
from dataclasses import dataclass, field


def number_to_xy(number):
    col = (number - 1) // 3
    lin = (number - 1) % 3
    return col, lin


DEFAULT_NUMBERS = (
    0,
    32,
    15,
    19,
    4,
    21,
    2,
    25,
    17,
    34,
    6,
    27,
    13,
    36,
    11,
    30,
    8,
    23,
    10,
    5,
    24,
    16,
    33,
    1,
    20,
    14,
    31,
    9,
    22,
    18,
    29,
    7,
    28,
    12,
    35,
    3,
    26,
)

DEFAULT_RED_NUMBERS = tuple(DEFAULT_NUMBERS[1::2])
DEFAULT_BLACK_NUMBERS = tuple(DEFAULT_NUMBERS[2::2])


class InvalidBet(Exception):
    pass


def distance_2d(a, b):
    p = tuple_sub(a, b)
    return (p[0] ** 2 + p[1] ** 2) ** 0.5


def tuple_sub(a, b):
    return tuple(
        a_item - b_item
        for a_item, b_item
        in zip(a, b)
    )


def range_bets():
    yield '1:12'
    yield '13:24'
    yield '25:36'
    yield '1:18'
    yield '19:36'
    yield from [f'{i + 1}:{i + 3}' for i in range(0, 35, 3)]
    yield from [f'{i + 1}:{i + 6}' for i in range(0, 32, 3)]


def range_bets_set():
    return set(range_bets())


@dataclass
class BetFactory:

    range_bets: Set[str] = field(default_factory=range_bets_set)

    def create_from_range(self, value: float, initial: int, final: int):
        return Bet.create_from_range(value, range(initial, final + 1))

    def create_from_str(self, value: float, raw_str):
        if raw_str in self.range_bets:
            a, b = raw_str.split(':', 1)
            return self.create_from_range(value, int(a), int(b))
        numbers = set(map(int, raw_str.split(',')))
        positions = list(map(number_to_xy, numbers))
        total_distance = distance_2d(max(positions), min(positions))
        total_numbers = len(numbers)
        if total_numbers == 1:
            return Bet(value=0, numbers=numbers)
        if total_numbers == 2 and total_distance <= 1:
            return Bet(value=0, numbers=numbers)
        if total_numbers == 4 and total_distance <= 1.5:
            return Bet(value=0, numbers=numbers)
        raise InvalidBet()


@dataclass
class Bet:
    value: float
    numbers: Sequence[int]

    @classmethod
    def create_black(cls, value: float):
        return cls(value=value, numbers=set(DEFAULT_BLACK_NUMBERS))

    @classmethod
    def create_red(cls, value: float):
        return cls(value=value, numbers=set(DEFAULT_RED_NUMBERS))

    @classmethod
    def create_from_filter(cls, value: float, fn):
        return cls(
            value=value,
            numbers=set(filter(fn, DEFAULT_NUMBERS[1:]))
        )

    @classmethod
    def create_odd(cls, value: float):
        return cls.create_from_filter(value, lambda it: it % 2 == 1)

    @classmethod
    def create_even(cls, value: float):
        return cls.create_from_filter(value, lambda it: it % 2 == 0)

    @classmethod
    def create_from_range(cls, value: float, range):
        return cls(value, set(range))

    @classmethod
    def create_low(cls, value):
        return cls.create_from_range(value, range(1, 19))


class Roleta:
    pass
