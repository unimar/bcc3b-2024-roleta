# Generated by Django 5.0.2 on 2024-04-17 22:52

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


def create_wallets(apps, schema_editor):
    User = apps.get_model(settings.AUTH_USER_MODEL)
    Wallet = apps.get_model('lasvegas.Wallet')
    for user in User.objects.all():
        Wallet.objects.create(owner=user)


def delete_wallets(apps, schema_editor):
    Wallet = apps.get_model('lasvegas.Wallet')
    Wallet.objects.filter(owner__isnull=False).delete()


class Migration(migrations.Migration):

    dependencies = [
        ("lasvegas", "0001_initial"),
    ]

    operations = [
        migrations.RunPython(create_wallets, delete_wallets)
    ]
